<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends MY_Controller {

	
	public function index()
	{	
		$this->load->view('header_page');
		$this->load->view('landing_page');
		$this->load->view('footer_page');
	}
}
