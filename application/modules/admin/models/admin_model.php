<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
 
  class Admin_model extends CI_Model{

    private $_table = "admin";

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function get_by_id_admin($id) {
        $this->db->from('admin');
        $this->db->where('id_admin',$id);
        $query = $this->db->get();

        return $query->row();
    }

  }