<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
 
 
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Admin
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Manage Admin</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="box">
            <div class="box-header">
              <h3 class="box-title">
              <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
              tambah admin
              </button>
              </button></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>id_admin</th>
                  <th>Username</th>
                  <th>Password</th>
                  <th>Nama Lengkap</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=1; foreach ($listnya as $admin): ?>
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $admin->id_admin ?></td>
                  <td><?php echo $admin->username ?></td>
                  <td><?php echo $admin->password ?></td>
                  <td><?php echo $admin->nama_lengkap ?></td>
                  <td>
                  <div class="btn-group" role="group" aria-label="...">
                    <button type="button" class="btn btn-danger" onclick="delete_admin(<?php echo $admin->id_admin ?>)">Delete</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" onclick="edit_admin(<?php echo $admin->id_admin ?>)" data-target="#edit_admin">Update</button>
                  </div>
                </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
  Launch demo modal
</button>

<!-- Modal tambah -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Admin</h4>
      </div>
      <div class="modal-body">
      <form id="form_admin" name="form_admin">
        <div class="form-group" id="form_admin">
            <label for="exampleInputEmail1">Nama Lengkap</label>
            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="nama_lengkap" name="nama_lengkap">
        </div>
        <div class="form-group">
            <label for="">Username</label>
            <input type="text" class="form-control" id="" placeholder="Username" name="username">
        </div>
        <div class="form-group">
            <label for="">Password</label>
            <input type="password" class="form-control" id="" placeholder="Password" name="password">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="tambah_admin()" >Save</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal edite -->
<div class="modal fade" id="edit_admin" tabindex="-1" role="dialog" aria-labelledby="edit_admin" data-target="#edit_admin">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Admin</h4>
      </div>
      <div class="modal-body">
      <form id="form_edit_admin">
        <div class="form-group" >
            <label for="">id_admin</label>
            <input type="hide" class="form-control" id="id_admin" placeholder="" name="id_admin">
        </div>
        <div class="form-group" >
            <label for="exampleInputEmail1">Nama Lengkap</label>
            <input type="text" class="form-control"  placeholder="" name="nama_lengkap">
        </div>
        <div class="form-group">
            <label for="">Username</label>
            <input type="text" class="form-control"  placeholder="" name="username">
        </div>
        <div class="form-group">
            <label for="">Password</label>
            <input type="password" class="form-control"  placeholder="" name="password">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="save_edit_admin()">Save</button>
        </form>
      </div>
    </div>
  </div>
</div>

  