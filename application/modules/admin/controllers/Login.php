<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

  public function index()
  {
    $this->load->view('login_web');
  }

  function login_acces(){
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $this->db->where('username', $username);
    $this->db->where('password', md5($password));
    $query = $this->db->get('admin');
  
    if($query->num_rows() > 0){
      $row = $query->row();
      $data_session = array(
          'id_admin'    => $row->id_admin,
          'nama_lengkap'  => $row->nama_lengkap,
          'logged_in'  	=> "login",
          );
      $this->session->set_userdata($data_session);
      redirect(base_url("admin"));
      }else{
      redirect(base_url("admin/login"));
      }
  }

}