<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	// function yang akan di proses pertama kali atau di jalankan
	function __construct(){
		parent::__construct();
		// load model
		$this->load->model("Admin_model");
		$this->load->library('encryption');
		// cek apakah admin telah melakukan login atau tidak, jika tidak maka funtion/middleware pada admin tidak berkerja
		if($this->session->userdata('logged_in') != "login"){
			// jika tidak melakukan login maka akan di alihkan ke halaman login
			redirect(base_url("admin/login"));
		}
	
	}

	// tampilkan halaman dashboard admin
	public function index()
	{
		//tampilkan bagian header web
		$this->load->view('header_web');
		// tampilkan sidebar atau menu admin
		$this->load->view('sidebar_web');
		// tampilkan content pada website atau isi tampilan dashboard, dapat di rubah sesuai fungsi atau tujuan
		$this->load->view('dashboard_web');
		// tempilkan footer pada website
		$this->load->view('footer_web');
	}

	// tampilkan halaman dashboard admin
	public function list_admin()
	{
		// ambil data dari model, kemudian kirim ke view
		$data["listnya"] = $this->Admin_model->getAll();
		//tampilkan bagian header web
		$this->load->view('header_web');
		// tampilkan sidebar atau menu admin
		$this->load->view('sidebar_web');
		// tampilkan content pada website , rubah sesuai isi halaman
		$this->load->view('manage_admin', $data);
		// tempilkan footer pada website
		$this->load->view('footer_web');
	}

	public function tambah_admin()
        {
			$data['nama_lengkap']=$this->input->post('nama_lengkap');
			$data['username']=$this->input->post('username');
			$data['password']= md5($this->input->post('password'));
        	$this->db->insert('admin',$data);
		}
	
	// nampilkan data yang di edite
	public function show_edit($id) 
		{
            $data = $this->Admin_model->get_by_id_admin($id);
            echo json_encode($data);
		}
		
	// simpan edit admin
	public function save_edit_admin()
        {
        	$id_admin=$this->input->post('id_admin');
			$data['nama_lengkap']=$this->input->post('nama_lengkap');
			$data['username']=$this->input->post('username');
			$data['password']=md5($this->input->post('password'));
        	$this->db->where("id_admin",$id_admin);
        	$this->db->update("admin",$data);
        }

	public function delete_admin()
    {
        $id_admin = $this->input->post('id_admin');
        $this->db->where('id_admin', $id_admin );
        $this->db->delete('admin');
    }

	// function logout
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('admin/login'));
	  }
	
}
